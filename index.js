// BAI 1: TINH LUONG NHAN VIEN

//input: salaryPerDay = 100000
const salaryPerDay = 100000;
let workDay = 10;
// progress
salary = salaryPerDay * workDay;
// output: salary ???
console.log('Salary:', salary);

// BAI 2: TINH GIA TRI TRUNG BINH
//input: a, b, c ,d, e
let a, b, c, d, e;
a = 5;
b = 2;
c = 3;
d = 4;
e = 9;
// progress
avgNum = (a + b + c + d + e) / 5;
// output: avgNum = ???
console.log('So trung binh:', avgNum);

//BAI 3:
// input: usd, vnd
let vnd = 23500;
let usd = 3;
//progress
money = usd * vnd;
// output: money???
console.log('Money', money, 'vnd');

//BAI 4:
//input: chieuDai, chieuRong
let chieuDai = 5;
let chieuRong = 10;
//progress
chuVi = (chieuDai + chieuRong) * 2;
dienTich = chieuDai * chieuRong;
// output: chuVi, dienTich ??
console.log('chuVi:', chuVi);
console.log('dienTich', dienTich);

//BAI 5 : TINH TONG 2 KY SO
//input: so nhap vao
let num1 = 25;
//progress
hangChuc = Math.floor(num1 / 10); //ROUDNED DOWN
hangDonVi = num1 % 10;
sum = hangChuc + hangDonVi;
//output: Sum cua cac chu so
console.log('Sum:', sum);
